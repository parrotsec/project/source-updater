#[
  Save logs and print information to screen
]#

import std/logging
import utils
import times
import os

type
  LogHandler* = object
    print*: ConsoleLogger
    file*: FileLogger


proc ulog_init*(log_root_dir: string): LogHandler =
  let
    date_format = now().format("dd-MM-yyyy")
    log_dir = uutils_standardized_dirname(log_root_dir & date_format)
    log_file = log_dir & "full.log"

  discard existsOrCreateDir(log_dir)

  var
    file_logger = newFileLogger(log_file, fmtStr = "$levelname [$date $time] ")
    console_logger = newConsoleLogger(fmtStr = "$levelname [$date $time] ")
    logger = LogHandler(
      print: console_logger,
      file: file_logger
    )

  return logger


proc ulog_info*(logger: LogHandler, name, msg: string, verbose = true) =
  # bright cyan
  let
    fullMsg = "\e[96m[" & name & "] " & msg & "\e[0m"

  if verbose:
    logger.print.log(lvlInfo, fullMsg)
  logger.file.log(lvlInfo, fullMsg)


proc ulog_updated*(logger: LogHandler, name, version: string) =
  # bright magenta
  let
    fullMsg = "\e[95m[" & name & "] Updated to " & version & "\e[0m"

  logger.print.log(lvlInfo, fullMsg)
  logger.file.log(lvlInfo, fullMsg)


proc ulog_debug*(logger: LogHandler, print_verbose: bool, name, msg, output: string) =
  let
    fullMsg = "[" & name & "] " & msg

  logger.print.log(lvlDebug, fullMsg)
  if print_verbose:
    logger.print.log(lvlDebug, output)

  logger.file.log(lvlDebug, fullMsg)
  logger.file.log(lvlDebug, output)


proc ulog_error*(logger: LogHandler, print_verbose: bool, name, msg, output: string) =
  let
    fullMsg = "\e[97m[" & name & "] " & msg & "\e[0m"

  logger.print.log(lvlError, fullMsg)
  if print_verbose:
    logger.print.log(lvlError, output)

  logger.file.log(lvlError, fullMsg)
  logger.file.log(lvlError, output)
