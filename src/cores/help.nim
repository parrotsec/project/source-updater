proc uupdate_short_help*() =
  #[
    Show fast help
  ]#
  echo "Usage: \e[33m./updater\e[0m <\e[96mcommand\e[0m> [\e[95mpackages\e[0m]"
  echo "  Or use: \e[33m./updater \e[96m-h\e[0m for full help"


proc uupdate_full_help*() =
  #[
    Show full description and command how it works
    # TODO show -v in help
  ]#
  echo "Description:"
  echo "  This tool uses the \e[92mgit-buildpackage\e[0m (command \e[92mgbp\e[0m) for updating projects automatically."
  echo "  This is used for Parrot OS to auto update the security tools from original source code"
  echo "  using uscan and push the code with tarball to the Parrot OS gitlab."
  echo "This project CAN'T do:"
  echo "  - Update copyright"
  echo "  - Update new dependencies for projects"
  echo "  - Solve package build issue"
  echo "Command structure:"
  echo "  \e[33m./updater\e[0m <\e[96mcommand\e[0m> [\e[95mpackages\e[0m]"
  echo "  * Packages is an optional argument. You can run the tool with either no package name,"
  echo "    1 package name or multiple package names. If you pass no name, the tool will"
  echo "    do the same command for all packages."
  echo "  * Command list:"
  echo "    - \e[96mclone\e[0m   Use \e[92mgbp clone\e[0m to clone project from Parrots repository with all branches."
  echo "              Command \e[96mclone\e[0m requires at least 1 package name."
  echo "    - \e[96mpull\e[0m    Use \e[92mgbp pull\e[0m to sync project with Parrots repository."
  echo "    - \e[96mupdate\e[0m  1. Use \e[92mgbp pull\e[0m to sync project."
  echo "              2. Use \e[92muscan --no-download\e[0m to check for update, get version number"
  echo "              3. Use \e[92mgbp import-orig --uscan\e[0m check for update and download new update."
  echo "              4. Use \e[92mgbp dch\e[0m to create new Debian's changelog."
  echo "              5. Use \e[92mgbp tag\e[0m to create new tag."
  echo "              6. Use \e[92mgbp push\e[0m to push all changes with all branches."
  echo "Config file:"
  echo "  Config file is allocated at same folder with binary (for now). Config file has:"
  echo "  - Required fields:"
  echo "     + \e[96mgit_url\e[0m Github URL, must be SSH protocol, to clone, pull and push project"
  echo "     + \e[96mroot_path\e[0m Parent folder of projects will be updated. If value is empty"
  echo "       This tool will create a folder \e[33mpackages\e[0m at same folder of this tool."
  echo "     + \e[96mlog_path\e[0m Parent folder of log files will be saved. If value is empty"
  echo "       This tool will create a folder \e[33mlogs\e[0m at same folder of this tool."
  echo "Example:"
  echo "  - Update all projects: \e[33m./updater \e[96mupdate\e[0m"
  echo "  - Clone \e[95mwpscan\e[0m:  \e[33m./updater \e[96mclone \e[95mwpscan\e[0m"
  echo "  - Pull \e[95mmetasploit-framework\e[0m and \e[95mwpscan\e[0m: \e[33m./updater \e[96mpull \e[95mmetasploit-framework wpscan\e[0m"
